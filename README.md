# Building a Review App for scanning with DAST

The purpose of this project is to demonstrate how to deploy a container using GKE for usage in [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/). Once configured, any new branches will deploy a container, start a review app, execute a DAST scan, then stop the environment.

## Prerequisite

Before deploying this project, a number of steps must be completed in order to prepare the Review Apps environment.

### Deploy Tokens

For GKE to be able to access the registry.gitlab.com container registry, a [Deploy Token]() must be created. 

To create a Deploy Token:

- Settings -> Repository -> Deploy Tokens
- Create a descriptive name
- Optionally set an expiration date
- Create a username of `gitlab-deploy-token`
- Click `Create deploy token`


<kbd>![deploy token](./img/deploy-token.png)</kbd>

It is recommended to give the Deploy Token the `gitlab-deploy-token` username so the CI variables will be automatically available as `$CI_DEPLOY_USER` and `$CI_DEPLOY_PASSWORD`.

After creating the deploy token, save the generated token in a secure location, as it maybe needed for troubleshooting purposes.


Once created the deploy token should available in the Deploy Token section of the Repository settings.

<kbd>![deploy token created](./img/deploy-token-created.png)</kbd>

### Configuring Kubernetes

Now that a deploy token has been created, we must integrate the Kubernetes cluster with GitLab. To begin the integration process, go to `Operations -> Kubernetes` and click `Integrate with a cluster certificate`.

<kbd>![integrate k8s](./img/integrate-k8s.png)</kbd>

Once clicked, there will be options to create a cluster on `Amazon EKS` or `Google GKE`. For this Review App we will be using GKE. Click the `Google GKE` option.

<kbd>![create GKE](./img/create-gke.png)</kbd>

There are a number of required fields to create the cluster. This also assumes the Google Cloud Platform has been linked to the GitLab account or instance.

The Kubernetes cluster name should be set to something specific for the project, in our case `review-app-gke-demo`.

For Environment scope, we will leave it to the default `*` so it will be used for all environments we create.

Select the appropriate Google Cloud Platform project, assuming it has been configured for the account.

Choose the appropriate GCP Zone, in our case we will use the default `us-central1-a`.


<kbd>![create cluster](./img/create-cluster.png)</kbd>

It may take a few minutes for the cluster to be created. 

### Configuring the cluster

Once the cluster has been created, it is possible to change the `Environment scope`, or set a `Base domain`. For this review app we are going to use the [nip.io](https://nip.io) Base domain and `Save changes`.

<kbd>![cluster details](./img/cluster-details.png)</kbd>

We then need to configure Ingress routing to our cluster. By using the Ingress application, we can easily allow routing to the Review Apps. Note that enabling this *may incur additional costs* and could be done by using other ingress/routing methods. 

Click on the `Applications -> Ingress -> Install`.

<kbd>![install ingress](./img/configure-ingress.png).</kbd>

It may take a few minutes for Ingress to install, after which we are ready to create our branches and configure our CI jobs.


## Review the gitlab-ci.yml 

The [.gitlab-ci.yml](./.gitlab-ci.yml) contains a number of necessary `kubectl` commands. Please review the comments prior to each command to ensure they apply to your specific application deployment.
